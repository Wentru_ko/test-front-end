import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase';

import Login from '@/pages/Login/Login'
import Dashboard from '@/pages/Dashboard/Dashboard'
import Detail from '@/pages/Detail/Detail'
import Profile from '@/pages/Profile/Profile'

Vue.use(Router)

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: {
        autentificado: true
      }
    },
    {
      path: '/detail/:userId',
      name: 'Detail',
      component: Detail,
      meta: {
        autentificado: true
      },
      props: true
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      meta: {
        autentificado: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  let usuario = firebase.auth().currentUser;
  let autorizacion = to.matched.some(record => record.meta.autentificado)

  if(autorizacion && !usuario) {
    next('login');
  } else if (!autorizacion && usuario) {
    next('dashboard');
  } else {
    next();
  }
})

export default router;
