import Vue from 'vue';
import App from './App.vue';
import router from './router';
import VueMoment from 'vue-moment';
import moment from 'moment-timezone';
import firebase from 'firebase';
import '@/includes/firebaseInit';
import store from '@/store';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import Vuex from 'vuex';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(Vuex)

Vue.use(VueMoment, {
  moment,
})

let app;

firebase.auth().onAuthStateChanged(function(user) {

  if(!app) {
    /* eslint-disable no-new */
    app = new Vue({
      el: '#app',
      router,
      store,
      components: {
        App
      },
      template: '<App/>'
    })
  }
});
