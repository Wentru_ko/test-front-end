import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import { ENDPOINT } from '@/includes/api.js';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        users: []
    },
    actions: {
        getUsers ({ commit }) {
        axios
            .get(ENDPOINT+ "/?results=10")
            .then(r => r.data)
            .then(users => {
                commit('SET_USERS', users)
            })
        }
    },
    mutations: {
        SET_USERS (state, users) {
            state.users = users;
        }
    }
})